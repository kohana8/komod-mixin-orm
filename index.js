const {K8} = require('@komino/k8');
K8.addNodeModules(require.resolve('./'));

module.exports={
  ControllerMixinMime: require('./classes/controller-mixin/Mime'),
  ControllerMixinMultiDomainDB : require('./classes/controller-mixin/MultiDomainDB'),
  ControllerMixinMultipartForm : require('./classes/controller-mixin/MultipartForm'),
  ControllerMixinORM: require('./classes/controller-mixin/ORM'),
  ControllerMixinView: require('./classes/controller-mixin/View'),
  HelperForm: require('./classes/helper/Form'),
}
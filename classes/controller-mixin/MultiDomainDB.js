const path = require('path');
const {K8, ControllerMixin} = require('@komino/k8');
const fs = require('fs');
const Database = require('better-sqlite3');
const url = require('url');
const DB = {pool : []};

class MultiDomainDB extends ControllerMixin{
  constructor(client) {
    super(client);

    const requestURL = url.parse('https://' + this.client.request.hostname);
    const hostname = requestURL.hostname || 'localhost';

    //guard hostname not registered.
    if(!fs.existsSync(path.normalize(`${K8.EXE_PATH}/../sites/${hostname}`))){
      this.client.notFound(`404 / store ${hostname} not registered`);
      return;
    }

    const conn = MultiDomainDB.getConnections(hostname);
    this.addBehaviour('hostname', hostname);
    this.addBehaviour('databases', conn);
    this.addBehaviour('db', conn.content);
  }


  static getConnections(hostname){
    if(!K8.config.cache.database || !DB.pool[hostname]){
      const dbPath = path.normalize(`${K8.APP_PATH}/../../sites/${hostname}/db`);

      DB.pool[hostname] = {
        content     : new Database(dbPath + '/content.sqlite'),
        transaction : new Database(dbPath + '/transaction.sqlite'),
        customer    : new Database(dbPath + '/customer.sqlite'),
        admin       : new Database(dbPath + '/admin.sqlite'),
        access_at   : Date.now()
      };
    }

    const connection = DB.pool[hostname];
    connection.access_at = Date.now();
    return connection;
  }
}

module.exports = MultiDomainDB;
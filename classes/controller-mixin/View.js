const {View, ControllerMixin} = require('@komino/k8');

class ControllerMixinView extends ControllerMixin{
  async before(){
    const view = this.getView(this.client.layout || 'layout/default', {});

    this.addBehaviour('view', view);
    this.addBehaviour('getView', this.getView);
  }

  async after(){
    const layout = this.client.mixin.get('view');
    if(!layout)return;

    if(this.client.tpl){
      layout.data.main = await this.client.tpl.render();
    }else{
      layout.data.main = this.client.body;
    }

    this.client.body = await layout.render();
  }

  getView(path, data, viewClass = null){
    if(viewClass)return new viewClass(path, data);
    return new View.defaultViewClass(path, data);
  }
}

module.exports = ControllerMixinView;
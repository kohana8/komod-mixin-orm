const {K8, ControllerMixin} = require('@komino/k8');
const fs = require('fs');
const qs = require('qs');
const HelperForm = K8.require('helper/Form');

class MultipartForm extends ControllerMixin{

  async before(){
    this.addBehaviour('$_GET', this.client.request.query || {});
    //no request body
    if( !this.client.request.body && !this.client.request.multipart){
      return;
    }


//    if(typeof this.client.request.body )
    const postData = (typeof this.client.request.body === 'object') ?
      Object.assign({}, this.client.request.body) :
      qs.parse(this.client.request.body);

    if(/multipart\/form-data/.test(this.client.request.headers['content-type'])){
      await HelperForm.parseMultipartForm(this.client.request, postData);
    }

    this.addBehaviour('$_POST', postData);
  }
}

module.exports = MultipartForm;
const {ControllerMixin} = require('@komino/k8');

class ControllerMixinORM extends ControllerMixin{
  async before(){
    this.client.id = (this.client.request.params.id) ? parseInt(this.client.request.params.id) : null;
    this.clientDataBase = this.client.mixin.get('db');
  }

  action_index(){
    if(!this.client.model)return;

    const m = new this.client.model(null, {database: this.clientDataBase});
    const instances = m.all();
    this.addBehaviour('instances', instances);
  }

  action_read(){
    if(!this.client.model)return;

    const instance = new this.client.model(this.client.id, {database: this.clientDataBase});
    this.addBehaviour('instance', instance);
  }
}

module.exports = ControllerMixinORM;
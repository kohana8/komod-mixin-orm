const {ControllerMixin} = require('@komino/k8');

class Mime extends ControllerMixin{
    before(){
        const matchExtension = (/\.[0-9a-z]+($|\?)/i).exec(this.client.request.raw.url || '');

        const extension = matchExtension ? matchExtension[0].replace(/[.?]/g, '') : 'html';
        this.addBehaviour('fileExtension', extension);

        switch(extension){
            case 'js':
            case 'json':
                this.client.headers['Content-Type'] = 'application/json; charset=utf-8';
                break;
            case 'png':
                this.client.headers['Content-Type'] = 'image/png';
                break;
            case 'jpg':
            case 'jpeg':
                this.client.headers['Content-Type'] = 'image/jpeg';
                break;
            case 'html':
                this.client.headers['Content-Type'] = 'text/html; charset=utf-8';
                break;
            default:
                this.client.headers['Content-Type'] = 'text/html; charset=utf-8';
        }
    }
}

module.exports = Mime;